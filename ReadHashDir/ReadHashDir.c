// ReadHashDir.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "sha3.c"


BOOL DirectoryExists(LPCTSTR szPath)
{
	DWORD dwAttrib = GetFileAttributesA(szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

BOOL Dir2(LPCTSTR szPath)
{
	DWORD dwAttrib = GetFileAttributes(szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

void printDir(TCHAR *searchString, char *path, size_t pathLen) {
	TCHAR filepath[MAX_PATH * 2], searchString2[MAX_PATH *2];;
	size_t pathLen2 = 0;
	HANDLE handle;
	char path2[MAX_PATH * 2];
	WIN32_FIND_DATA file;
	sha3_context context;
	uint8_t *hash, fileBuffer[4096];
	size_t bytesRead = 0;
	int j;
	char *str, hash256[65], *c2[2];

	handle = FindFirstFile(searchString, &file);

	if (handle != INVALID_HANDLE_VALUE) {
		FindNextFile(handle, &file);
		while (FindNextFile(handle, &file) != 0) {
			mbstowcs(filepath, path, pathLen);
			filepath[pathLen] = 0;
			wcscat(filepath, L"\\");
			wcscat(filepath, file.cFileName);

			if (Dir2(filepath) == TRUE)
			{
				wcscpy(searchString2, filepath);
				pathLen2 = wcslen(filepath);
				wcstombs(path2, filepath, pathLen2);
				path2[pathLen2] = '\0';
				wcscat(searchString2, L"\\*\0");
				printf("%s\r", path2);
				printDir(searchString2, path2, pathLen2);
			}
			else{
				char usablePath[MAX_PATH];
				wcstombs(usablePath, filepath, MAX_PATH);
				FILE *fp = fopen(usablePath, "rb");
				sha3_Init256(&context);
				while ((bytesRead = fread(fileBuffer, 1, sizeof(fileBuffer), fp)) > 0) {
					sha3_Update(&context, fileBuffer, bytesRead);
				}
				hash = sha3_Finalize(&context);
				for (j = 0; j < 32; j++) {
					sprintf(c2, "%02X", hash[j]);
					strncpy(&hash256[j * 2], c2, 2);
				}

				hash256[j * 2] = '\0';
				printf("%s\t%s\r", usablePath, hash256);

				fclose(fp);
			}
		}
	}
}

int main(int argc, char *argv[])
{
	char *path;
	if (argc > 1) {
		path = argv[1];
		
		if (DirectoryExists(path) == TRUE) {
			size_t pathLen = 0;
			TCHAR searchString[100];

			pathLen = strlen(path);
			mbstowcs(searchString, path, pathLen);
			searchString[pathLen] = 0;
			wcscat(searchString, L"\\*\0");

			printDir(searchString, path, pathLen);
		}
		else {
			printf("Not a valid directory");
			return 1;
		}
	}
	else {
		printf("Requires a path");
		return 2;
	}
	return 0;
}

